export default function CloseIcon({ onClicked }) {
  return (
    <svg
      width="48"
      height="40"
      onClick={onClicked}
      viewBox="0 0 48 40"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect x="8" width="40" height="40" rx="6" fill="white" />
      <path
        d="M22 26L34 14M22 14L34 26"
        stroke="#9CA3AF"
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </svg>
  );
}
