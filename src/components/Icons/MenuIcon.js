export default function MenuIcon({ onClick, ...props }) {
  return (
    <svg
      {...props}
      onClick={onClick}
      width="48"
      height="40"
      viewBox="0 0 48 40"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect x="8" width="40" height="40" rx="6" fill="#111827" />
      <path
        d="M20 14H36M20 20H36M20 26H36"
        stroke="#9CA3AF"
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </svg>
  );
}
