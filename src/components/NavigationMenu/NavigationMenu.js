import { PRIMARY_COLOR_LIGHT } from "../../constants/colors";
import Logo from "../Icons/Logo";
import styles from "./NavigationMenu.module.css";
import CloseIcon from "../Icons/CloseIcon";

export default function NavigationMenu({ open, onClose }) {
  return (
    <aside className={[styles.menu, open ? styles.open : ""].join(" ")}>
      <div className={styles.card}>
        <header className={styles.card_header}>
          <Logo color={PRIMARY_COLOR_LIGHT} />
          <CloseIcon onClicked={onClose} />
        </header>
        <nav className={styles.card_content}>
          <ul className={styles.list}>
            <li>Product</li>
            <li>Features</li>
            <li>Marketplace</li>
            <li>Company</li>
          </ul>
        </nav>
        <div className={styles.card_actions}>
          <button className={styles.button}>Start free trial</button>
          <p className={styles.helper_text}>
            Existing customer? <a href="/login">Login</a>
          </p>
        </div>
      </div>
    </aside>
  );
}
