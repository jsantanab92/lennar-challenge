import { PRIMARY_COLOR } from "../../constants/colors";
import Logo from "../Icons/Logo";
import MenuIcon from "../Icons/MenuIcon";
import Spacer from "../Spacer/Spacer";
import styles from "./Header.module.css";

export default function Header({ onMenuClicked }) {
  return (
    <header className={styles.header} aria-label="Main Header">
      <div className={styles.logo_container} aria-label="Logo">
        <Logo color={PRIMARY_COLOR} aria-hidden="true" />
      </div>

      <nav className={styles.nav} aria-label="Main navigation">
        <ul className={styles.list}>
          <li>Product</li>
          <li>Features</li>
          <li>Marketplace</li>
          <li>Company</li>
        </ul>
      </nav>

      <Spacer />

      <div className={styles.actions}>
        <button aria-label="Login" className={styles.login_button}>
          Login
        </button>
        <button
          aria-label="Start free trial"
          className={styles.free_trial_button}
        >
          Start free trial
        </button>
      </div>

      <button className={styles.menu_button} onClick={onMenuClicked}>
        <MenuIcon aria-hidden="true" />
      </button>
    </header>
  );
}
