import styles from "./Backdrop.module.css";

export default function Backdrop({ onClicked }) {
  return (
    <div
      role="button"
      tabIndex="0"
      aria-label="Close menu"
      onClick={onClicked}
      className={styles.backdrop}
    />
  );
}
