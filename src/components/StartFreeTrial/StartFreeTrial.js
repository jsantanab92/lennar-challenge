import styles from "./StartFreeTrial.module.css";

export default function StartFreeTrial() {
  const handleSubmit = (e) => {
    e.preventDefault();
  };
  return (
    <div className={styles.container}>
      <form className={styles.form} onSubmit={handleSubmit}>

        <input
          type="email"
          id="email"
          name="email"
          className={styles.input}
          placeholder="Enter your email"
          required
          pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$"
          title="Please enter a valid email address."
        />

        <button className={styles.button} type="submit">
          Start free trial
        </button>

      </form>

      <p className={styles.helpertext}>
        Start your free 14-day trial, no credit card necessary. By providing
        your email, you agree to our{" "}
        <a href="/terms-of-service">terms or service</a>.
      </p>
      
    </div>
  );
}
