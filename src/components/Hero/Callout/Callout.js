import ChevronRight from "../../Icons/ChevronRight";
import styles from "./Callout.module.css";

export default function Callout() {
  return (
    <div
      className={styles.container}
      role="complementary"
      aria-label="Job Hiring Callout"
    >
      <div className={styles.badge}>
        <h6 aria-label="Job Alert">we're hiring</h6>
      </div>
      <a
        rel="noopener noreferrer"
        className={styles.link}
        target="_blank"
        href="/careers"
      >
        Visit our careers page
      </a>
      <ChevronRight aria-hidden="true" />
    </div>
  );
}
