import Callout from "./Callout/Callout";
import styles from "./Hero.module.css";

export default function Hero() {
  return (
    <div className={styles.container} role="banner">
      <Callout />
      <h1
        className={styles.main_text}
        aria-label="A better way to ship web apps"
      >
        A better way to <br aria-hidden="true" />
        <span className={styles.main_text_remark}>ship web apps</span>
      </h1>
      <p
        className={styles.supporting_text}
        aria-label="Description of the service"
      >
        Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui Lorem
        cupidatat commodo. Elit sunt amet fugiat veniam occaecat fugiat.
      </p>
    </div>
  );
}
