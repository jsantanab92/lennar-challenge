import React, { useState } from "react";
import Header from "./Header/Header";
import NavigationMenu from "./NavigationMenu/NavigationMenu";
import Backdrop from "./Backdrop/Backdrop";

const Layout = ({ children }) => {
  const [open, setOpen] = useState(false);

  const handleToggleDrawer = () => {
    setOpen(!open);
  };

  return (
    <>
      <Header onMenuClicked={handleToggleDrawer} />
      {open && <Backdrop onClicked={handleToggleDrawer} />}
      <NavigationMenu open={open} onClose={handleToggleDrawer} />
      <main>{children}</main>
    </>
  );
};

export default Layout;
