import Hero from "./components/Hero/Hero";
import Layout from "./components/Layout";
import StartFreeTrial from "./components/StartFreeTrial/StartFreeTrial";
import styles from "./App.module.css";

function App() {
  return (
    <Layout>
      <div className={styles.container}>
        <div className={styles.inner_container}>
          <Hero />
          <StartFreeTrial />
        </div>
      </div>
    </Layout>
  );
}

export default App;
